  $(onLoad)

  function onLoad() {

    $('#btn').on('click', getPostAndComments);

    function getPostAndComments() {
      $.ajax({
          url: 'https://jsonplaceholder.typicode.com/posts'
      })
      // Aducere primele 10 posturi
      .then(function(resultPosts) {
        var html = '<ul>'                                                           // Afisare titlu posturi intr-o lista
        for (var i = 0; i < 10; i++) {
          var posts = resultPosts[i]
          html += '<li>';
          html += '<div>' + posts.title + '</div>';                              // var postTitle = resultPosts[i].title si var postsId = resultPosts[i].id -> 
          html += '<ol id="post_id_' + posts.id + '"></ol>';                    // am dat id la <ol> ca sa se afiseze in ordine crescatoare, nu rendam
          html += '</li>'
          getCommentsPost(posts.id);   // lista goala ptr ca inca nu avem comentarile   // l-am pus aici ptr a avea siguranta ca posturiile se creaza primele si dupa ce sunt create listele, sunt puse si commentuuirle      
        }
        html += '</ul>'
        $('.content').html(html);
      })
    }
    
    function getCommentsPost(postId) {
      $.ajax({
          url: 'https://jsonplaceholder.typicode.com/posts/' + postId + '/comments',
      })
      .then(function(resultComments) {
        var commentsList = '';
        var maxComments = resultComments > 10 ? 10 : resultComments.length;               // daca resultComments este mai mare de 10, atunci afiseaza doar primele 10 primele, daca nu este, atunci afiseaza lungimea lui          
        for (i = 0; i < maxComments; i++) {
          commentsList += '<li>' + resultComments[i].name + '</li>';
        }
        $('#post_id_' + postId).html(commentsList);
         //console.log('Comments are: ', resultComments);
      })
      .catch(function(error) {
        $('#post_id_' + postId).html('<li style="color:red"> Cannot get comments for post id ' + postId + '</li>')
      })
    }

  }