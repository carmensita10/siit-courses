 window.addEventListener("load", function(event) {
  console.log("All resources finished loading!");
 });

// when document obj is available                                                // toate scripturiile intra aici, intre acolade
   document.addEventListener("DOMContentLoaded", onHtmlLoaded);                 // functie pe obiectul document
   function onHtmlLoaded() {                                                   // documentul DOM este incarcat
    console.log("DOM fully loaded and parsed");
   
// Identificare element 

   // by id
     console.log("Comments list - by ID", document.getElementById('comments-list'));
   // by class
     console.log("Comments items - by Class", document.getElementsByClassName('comment-item'));                               // returneaza un array de elemente
   // first by class
     console.log("First comment - first by class", document.getElementsByClassName('comment-item')[0]);               
   // by tag name
     console.log("Comments items - by tag name", document.getElementsByTagName('section'));                                  // returneaza un array de elemente
   // Second by tag
     console.log("Second item - second tag name", document.getElementsByTagName('section')[1]);
   // first by class & tag
     console.log("First comment - first by class & tag", document.querySelector('section.comment-item'));                  // primul element care indeplineste conditiile
   // all by class & tag
    console.log("Comments items - all by class & tag", document.querySelectorAll('section.comment-item'));               // voi avea un array de elemente
   // all users
     console.log("Users for comments -  all users", document.querySelectorAll('section.comment-item strong'));
   
   
       // Curs continuare

   
    // Change h1 content
      var h1 = document.querySelector('h1');                                // h1 va fi suprascris
      h1.innerText = 'JS Manipulation';                                    // as string -> ptr ca este innerText
      console.log("H1 text", h1.innerText);                               // se recomanda sa existe doar un singur h1 pe pagina => este primul element care indeplineste conditia
      h1.innerHTML = 'JS Manipulation <em>New</em>';
      console.log("H1 HTML", h1.innerHTML);

   
    // Set Attribute
      var h2 = document.querySelector('h2'); 
      h2.setAttribute('title', 'Comments List');                               // set attribute 
      console.log('Get Attribute', h2.getAttribute ('title'));
      h2.id = "Comments-list-id";                                             // set id attribute. la ID si la CLASS se poate scrie direct variabila.atribut = "valoare"
      console.log("Comments list id is", h2.id);

   
    // Change style                                                                     // punem border la containerul div cu id="comments-list"
      var commentsContainer = document.getElementById('comments-list');
      commentsContainer.style.border = "solid 1px green";                             // suprascriu proprietatea css border
   
   
   // Add border to each comment
      var commentsItems = document.getElementsByClassName('comment-item');               // getElementsByClassName returneaza un array
      for (var i = 0; i < commentsItems.length; i++) {                                  // mergem pe fiecare element din array si suprascriem proprietatea respectiva
       commentsItems[i].style.border = "solid 2px red";
    // commentsItems.style.border = "solid 1px black";       -> array-ul nu are style, ci doar elementele din ele si din cauza asta nu putem suprascriem pe el. nu putem face selectii.
   }
   
   
   // Create new DOM elements ->  adaugare un nou comentariu 
     
     // Pasul 1: creare element de tip section in DOM / un nod de dom
       var commentItem = document.createElement('section');  
     // Pasul 2: Adaugare clasa section 
       commentItem.className = 'comment-item';                                                // variabila.ID = "valoare" sau variabila.className = "valoare"
     // Pasul 3: Adaugare continut
       commentItem.innerHTML = '<h3> New comment 3 </h3>' +                                      
                               '<p> New Comment content <strong> Daniel Rus </strong>';           
     // Pasul 4: Identificare locatie asezare nod                                          // unde in structura mea de HTML doresc sa pun acest sectiun? in lista de comentarii
        commentsContainer.appendChild(commentItem);                                       // commentsContainer este deja un DOM element
     // var commentsContainer = document.getElementById('comments-list');                // identficare lista de comentarii -> o avem un pic mai sus

     
   // Remove first comment item
     var commentItem = document.querySelector('section.comment-item');              // identificare prim element 
     commentsContainer.removeChild(commentItem);                                   // de unde vreau sa il sterg? din comments-list
   
     
     
   // Workshop - Add comments list
      // lista cu comentarii din DB care trebuie afisata in HTML
     // comentariu = titlul, continut, autor => obiect cu 3 proprietati/chei
    // voi avea un array cu comentarii
   
   var list = [    
     {
       title: "Title 1",
       body: "Lorem ipsum...",
       author: "Ioan Pop"
     }, 
      {
       title: "Title 2",
       body: "Lorem ipsum...",
       author: "Lorena Pop"
     }, 
      {
       title: "Title 3",
       body: "Lorem ipsum...",
       author: "Daniel Bolog"
     }
   ];
   
   // Adaugare comentarii in html 
     for(var i = 0; i < list.length; i++) {                                                                  // parcurgere lista de comentarii
       var comment = list[i];                                                                               // ptr fiecare el. din lista trebuie sa generez...
       var item = document.createElement('section');                                                       // creare el. html de tip section
       item.className = 'comment-item';                                                                   // unde trebuie pus? in clasa 'comment-item'
       item.innerHTML = '<h3>' + comment.title + '</h3>' +                                               // string dinamic = imi vine din parcurgerea array-ului
                        '<p>' + comment.body + '<strong>' + comment.author + '</strong><p>' 
       commentsContainer.appendChild(item);                                                            // adaugare in html in comments-list
     }
   
   
   // EVENTS
     var btnSearch = document.getElementById('btn-search');                        // elementul la care m*am abonat: btn-search
     btnSearch.addEventListener('click', onSearch);                               // evenimentul la care m-am abonat: click
     //  btnSearch.addEventListener('mouseover', onHover);                       // ne putem abona la mai multe evenimente pe elementul respectiv
     function onSearch() {                                                      // 
       alert(1);
     }
   
     
   // Dezabonare / unbind event
      // btnSearch.removeEventListener('click', onSearch);            // trebuie sa ii specific ce eveniment vreau sa il sterg si ce anume sa nu se mai intample dupa ce a fost accesat.
                                                                     // sa fie exact call back-ul

   
   
   
   

 }

// ma abonez la evenimentul de document loaded -> sa imi zica cand se termina de executat

  































