const numbers = [5, 7, -2, 4, 8, 9, 13];
let evens = []; // even numbers from numbers array will be added here

for (let i = 0; i < numbers.length; i++) {
  if (numbers[i] % 2 === 0) {
    evens.push(numbers[i]); // push adauga un element in coada array-ului
  }
}
console.log('after for: ', evens);

evens = [];
numbers.forEach(function(currentElement) {
  if (currentElement % 2 === 0) {
    evens.push(currentElement);
  }
});

console.log('after forEach: ', evens);

evens = [];
numbers.forEach(currentElement => { // ((currentElement) {}) -> si asa se poate scrie. dar este un singur argument si nu trebuie neaparat puse ()
  if (currentElement % 2 === 0) {
    evens.push(currentElement);
  }
});

console.log('after forEach with arrow function: ', evens);

// Lexical this -> avem acelasi this in array function ca si in functia de mai sus

window.onload = function() {
  const button = document.getElementById('my-element');

  this.clicked = 0;
  //   button.addEventListener('click', function() {
  //     console.log('this insede event handler ', this);
  //     this.clicked++;
  //     console.log("button clicked " + this.clicked + " time");
  //   });

//   const that = this;
//   button.addEventListener('click', function() {
//     console.log('this insede event handler ', this);
//     that.clicked++;
//     console.log("button clicked " + that.clicked + " time");
//   });
  
  button.addEventListener('click', () => {
    this.clicked++;                                                      // this.clicked++ is the same with the this.clicked = 0;
    console.log("button clicked " + this.clicked + " time");
  });
  
// CLASSES

console.log('-------CLASSES-----------');

class Animal { 
  constructor(options) {        // cand scriem new Animal() constructorul este cel apelat // in constr definim propoeitatile
  options = options || {};
    this.name = options.name;    // proprietati
    this.color =  options.color;
    this.weight = options.weight;
    this.height = options.height;
    this.legs = options.legs;
  }
  
  eat() {
    console.log("nom, nom animal is eating");
  }
  
  speak() {
    console.log("Hello, animal is speaking");
  }
  // define getter method
  get biometricData() {
    return "This animal is " + this.height + " tall and weights " + this.weight;
  }
  
  set biometricData(data){
    // data will have the format [height, weight]
    this.height = data[0];
    this.weight = data[1];
  }
  
  static SIT() {
    console.log("Sitting from a static method");
  }
}

  const fulga = new Animal({
    name: 'Fulga',
    color: 'purple',
    weight: '200kg',
    height: "1.80m",
    legs: 4
  });
  
  console.log("Fulga: ", fulga);
  
  // invoke class methods
  fulga.eat();
  fulga.speak();
  
  // use getter
  console.log(fulga.biometricData) // biometricData este proprietate, nu functie. get este functie
  
  fulga.biometricData = ["2m", "250kg"];
  console.log(fulga);
  
  // static function call
  // static methos are available in the class directly, and not on the instance
  Animal.SIT(); // not fulga.SIT();
  
  // Inheritance-> extends  Parent class-> se mostenesc metodele si proprietatiile
  console.log("--------Inheritance--------------");
  
  class Dog extends Animal {
    // constructor function is optional in child class
    constructor(options) {
      // ptr ca suntem intr-o clasa copil este obligatriu sa apelam super. super este o metoda prin care apelam constructorul clasei parinte
      super(options);
      this.legs = 4;
      this.breed = options.breed;
    }
    // method overriding
    speak() {
      console.log("Ham, Ham");
    }
  }
  
  const nero = new Dog({
    name: "Nero",
    color: "White and brown",
    height: "50cm",
    weight: "50kg",
    breed: "Ciobanesc Bucovinean"
  });
  
  console.log("Nero", nero);
  nero.speak();
};




