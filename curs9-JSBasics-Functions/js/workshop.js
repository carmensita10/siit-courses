 // Explicatii:
    // reprezentare in obiect
   // am mai multi clienti, mai multe proprietati- > trebuie folosit array
  // array de obiecte

// Pasul 1: reprezentarea datelor                                           
  var person1 = {                              
    name: "Ana",
    gender: "F",
    height: 170,
    weight: 55 
  };
  var person2 = { 
    name: "Ion",
    gender: "M",
    height: 185,
    weight: 95 
  };
  var persons = [person1, person2];                         // array cu elemente de tip obiect; obiecte care contin 4 proprietati
  console.log("Persons = ", persons);

// Pasul 2: Parcurgere array
  for(var i = 0; i < persons.length; i++) {                   // cand i=0, acceseaza person1, cand ii i=1, acceseaza person2
     var person = persons[i];                                // ptr fiecare pers trebuie sa calculez ceva -> parcurgere array
     console.log("Person = ", person);                      // cu "for" iteram un array si putem accesa elemen. din el 
     console.log("Height = ", person.height);
    
    var bmi = person.weight / (person.height * person.height); 
    var result = '';
    
   // daca nu am fi decalrat var person
     // console.log("Person = ", persons[i]);                 // in persons[i] avem obiectul, acceseaza obiectul persons de pe pozitia[i]
     // console.log("Height = ", persons[i].height);

  // Pasul 3: Interpretare date introduse de user
    if (bmi < 18.5) {
        result = 'underweight';
      } else if (bmi < 25) {
         result = 'normal';
      } else if (bmi < 30) {
         result = 'overweight';
      } else {
         result = 'obese';
      }
      console.log(person.name + ' | ' + person.gender + ' | ' + 'BMI: ' + bmi + ' | ' + result);
  }









