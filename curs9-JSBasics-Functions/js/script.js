      // Functiile si scopul lor

// Functie statement -> Functie clasica
  function myFirstFunction() {
    console.log('My first JS Function');
  }
    myFirstFunction();


// Functie care returneaza un rezultat si il afiseaza in consola
  function myFirstFunctionReturn() {
    return "My first Js Function Return";
  }
    var message = myFirstFunctionReturn();                             // functiile care returneaza valori vor fi salvate in variabile
    document.write(message);
    document.writeln('<strong>' + message + '</strong>');             // ln -> concatenare element html (care acum este string) cu o variabila (message). 


// Functie cu for statement 
  function displayStars() { 
    var line = "";                                 // linie goala
    for (var i = 0; i < 4; i++) {                 // generare randuri/linii de stelute
      line += "*";                               // concatenare * la linia anterioara
      console.log(line);
    }
  }
    displayStars();                          // ca sa nu scriem de doua ori scriptul, le-am bagat intr-o functie
    displayStars();


// Functie cu parametrii
  function displayStars1(nr) {              // pot specifica cate linii cu stelute sa imi genereze intr-un loc si cate in alta parte
    var line = ""; 
    for (var i = 0; i < nr; i++) { 
      line += "*"; 
      console.log(line);
    }
  }
    displayStars1(4);                     // generare 4 linii cu stelute
    displayStars1(2);
    displayStars1(9);


// Functie cu parametrii cu valoare default "="
  function displayStars2(nr = 4) {                   // l-am facut default ca sa nu scriu de fiecare data ca vreau sa genereze 4 linii
    var line = ""; 
    for (var i = 0; i < nr; i++) { 
      line += "*"; 
      console.log(line);
    }
  }
    displayStars2();                               // de 3 ori se afiseaza cu cate 4 linii
    displayStars2();
    displayStars2();
    displayStars2(2);                            // o data se afiseaza cu 2 linii


// Functie cu parametrii de tip string
  function displayStars3(nr = 5, char = '0') {
    var line = ""; 
    for (var i = 0; i < nr; i++) { 
      line += char; 
      console.log(line);
    }
  }
    displayStars3(2, '0');                // trebuie pastrata ordinea parametriilor
    displayStars3(4, '-');
    displayStars3(3, '*');


// Functie cu parametrii primitivi -> Sending parameters by value
  var fname = "Ana";
  var lname = "Pop";

  function displayName(fname, lname) {
    console.log(fname + " " + lname);
    fname = "Ion";
    console.log("Inside function fname ", fname);         // Ion
  }
    displayName(fname, lname);
    console.log("Outside function fname ", fname);       // Ana


// Functie cu  parametru de tip obiect -> Sending parameters by reference
  function displayNameObject(person) {                                       // parametru de tip obiect ->person
    console.log(person.fname + " " + person.lname);
    person.fname = "Ion";                                                  // suprascriere parametru fname
    console.log("Inside function fname", person.fname);                  // Ion
  }
// person object declaration
  var person = {
    fname: "Ana",
    lname: "Pop"
  }
    displayNameObject(person);
    console.log("Ouside function fname", person.fname);        // Ion 


    // Curs continuare


// Statement function -> functie clasica
  function statementFunction() {
    console.log("Statement function");
  }
    statementFunction();


// Expression function
  var expFunction = function() {
    console.log("Exp function");
  }
    expFunction();


// IIFE self invoking function ->  Immediately invoked function expression
  (function() {
    console.log("IIFE")
  })(); // apel de functie


// Function with function as parameter - > Call back function
  function testFunction() {
//  alert("Exec from another function");
  }
  function execOtherFunction(testFunction) {
    testFunction();                                       // 2. executare
  }
    execOtherFunction(testFunction);                     // 1. apelare


    // Scopul functiilor


// Varabile globale
  var a = 1; 
  b = 2; 
  window.c = 3;                                // window = obiect care contine info despre browser, iar varaibiele se salveaza in el


// Variabile locale
  function displayVariable() {
    var d = 4; 
    e = 5;
    console.log("Inside Variable d =", d);
    console.log("Inside Variable e =", e);
  }
    displayVariable();
 // console.log("Outside Variable d =", d);           // error -> d is not defined
    console.log("Outside Variable e =", e);


// Closure

  /* function a() {
      function b() {
       // code here                               // codul din functia b() nu poate fi accesat din exterior. 
      }                                          // poate fi accesat doar daca facem un return sau apel la function a() 
    }*/


// 3 Scoupe Layer Function
  var x = 1;                                            // global
  function firstF() {                                  // are acces la variabilele din interiorul ei(functia firstF) si la varabilele definite in scop global
    var y = 2; 
                                                      // local first function
    secondF();                                       // apelare secondF ca sa citeasca varabila w.
    function secondF() {                            // are acces la variabilele din interiorul functie secondF, la var din functia tata si la varabilele definite in scop global
      var z = 3;                                   // locala in secondF
      w = 4;
      console.log("Second x = ", x);             // 1 
      console.log("Second y = ", y);            // 2
      console.log("Second z = ", z);           // 3
    }
      console.log("First y = ", y);           // 2
      console.log("First x = ", x);          // returneaza 1 
   // console.log("First z = ", z);         // undefined
  }
    firstF();
    console.log("Global x = ", x);        // 1
 // console.log(" y = ", y);             // undefined ptr ca este locala.
 // console.log(" z = ", z);            // undefined
    console.log(" w = ", w);           // 4 -> w este var globala








