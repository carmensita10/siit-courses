console.log("DOM ", document);
console.log("DOM", window.document);

console.log("Console.log", console);
console.log("Console.log", window.console);

// open new tab with js
window.open('http://google.com');
// close tab open with window.open
// window.close();

// timing - execute code after 5 sec
setTimeout(function() {                       // functie care se afla pe window. are 2 parametrii- o functie de call back si timpul pe care vreau sa il astept dupa ce se executa functia respectiva    
  console.log("execute after 5 seconds");
}, 5000);                         