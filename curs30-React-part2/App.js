import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import GameList from './components/GameList.js';

class App extends Component {
  render() {
    return (
      <GameList />
    );
  }
}

export default App;
