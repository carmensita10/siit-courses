import React from "react";

import Game from "./Game.js";

class GameList extends React.Component {

    constructor() {
        super();                                               // call the parent class constructor - OOP

        this.state = {                                       // games este o proprietatea a lui state, iar state este o proprietate a clasei
            games: []                                       // this will be updated with data from the API
        };
    }

    componentWillMount() {
        fetch("https://games-world.herokuapp.com/games")           // perform API call
            .then((response) => {
                // this is fetch specific
                const data = response.json();                  // in data stau datele venite de la API
                console.log('Data', data);
                return data;
            })
            .then((data) => {                        // avem deja datele de la API
                this.setState({                     // cannot set the state by directly changing the state object
                    games: data                    // the games in the state object is the data received from the API
                });
            })
    }

    // randarea se face dupa ce componenta este creata
    // render este apelat automat, de fiecare data cand props sau state sunt modificate. props este input, nu il putem modifica noi.
    render() {                                                // metoda render afiseaza ce este randat
        const gameComponents = [];
        // gameElements.push(<Game game1={games[0]} />);
        // gameElements.push(<Game game1={games[1]} />);

        this.state.games.forEach(function (currentGame) {         // get the games data from the state
            gameComponents.push(<Game game1={currentGame} />);
        });

        /*for(let i=0; i<this.state.games.length; i++) {
            gameComponents.push(<Game game1={games[i]} />);   
        }*/
        
        return (
            <div>
                {gameComponents}
            </div>
        );
    }
}

export default GameList;
