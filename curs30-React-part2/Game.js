import React from "react"; 
function Game(props) {
  const game = props.game1;

  return (
    <div>
      <h2>{game.title}</h2>
      <p>{game.description}</p>
    </div>
  );
}

export default Game;
