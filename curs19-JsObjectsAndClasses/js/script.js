var emptyObject = {}; // no properties and no methods
console.log(emptyObject);

var emptyObject2 = new Object();
console.log(emptyObject2);

var userMe = {
  name: "Carmen Pop",
  height: 155,
  weight: 56,
  age: 25,
  //  age: 25 - duplicated key are not possible
  "eyes color": "brown",
  sayHi: function() { // method without param
    console.log("Hello world from an object method");
  },
  sayHiTo: function(name) { // method with param
    console.log("Hello " + name + "!!!");
  }
};

console.log(userMe);
userMe.sayHi();
userMe.sayHiTo("grupa 5");

console.log(userMe.name); // dot notation
console.log(userMe["eyes color"]); // brackets notation
console.log(userMe["name"]); // brackets notation

var prop = "height";
console.log("Height", userMe[prop]); // will display height
console.log(userMe[prop]);
console.log(userMe.prop); // the same as: userMe["prop"]

prop = "age";
console.log("Age", userMe[prop]); // will dispaly age


// Object Built-in Methods
console.log("userMe keys: ", Object.keys(userMe)); // userMe este obiect
console.log("userMe values: ", Object.values(userMe));

// sau
var keys = Object.keys(userMe);
for (var i = 0; i < keys.length; i++) {
  var prop = keys[i];
  console.log(prop, userMe[prop]); // prop este cheia, iar userMe[prop] este valoarea => name carmen pop // obj[prop]=value 
}

// Constructor function/clasa->Litera mare(conventie)

function User(options) {
  options = options || {} // daca nu ii dam noi nume, varsta etc (optiuni), va fi un empty object
  this.name = options.name; // this.name = options.name || "";  daca nu ii dam nume, o sa puna un string gol.   || "" -> Este folosit ca sa evitam eroriile de consola       
  this.age = options.age;
  this.height = options.height;
  this.weight = options.weight;
  this.calculateBMI = function() {
    return this.height / this.weight;
  }
}

var user1 = new User({
  name: "Mela",
  height: 155,
  weight: 55
});

console.log("User created with constructor function ", user1);
console.log("User BMI: ", user1.calculateBMI());

// adaugare nou user

var colleague = new User({
  name: "Andreea",
  height: 175,
  weight: 55
});

console.log("Colleague BMI: ", colleague.calculateBMI());

// Prototype property - pt function

console.log("User prototype: ", User.prototype); // stie ca ii constructor function ptr ca este scris cu litera mare si ptr ca are this-urile declarate in el

User.prototype.sayHi = function() {
  console.log("hi there from " + this.name); // accesam numele Mela si Andreea, iar apelarea este mai jos
}

user1.sayHi();  // aici sunt apelate
colleague.sayHi();

// Prototype attribute - pt obiecte.
console.log("there is no prototype property on object", user1.prototype)

console.log("Prototype attribute",
  Object.getPrototypeOf(user1),
  Object.getPrototypeOf(colleague));

console.log("constructor function prototype is prototype of object",
  User.prototype.isPrototypeOf(user1),
  User.prototype.isPrototypeOf(colleague));






