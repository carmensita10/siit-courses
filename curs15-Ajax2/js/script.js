$(domLoaded)

function domLoaded() {

  var getShowsData = function(DynamicValue) {                                  // ca si parametru puteam pune orice denumire
    $.ajax({                                                                   // obiect de tip ajax {chei:valoare}
      url: 'http://api.tvmaze.com/search/shows?q='+  DynamicValue,            
      method: 'GET',
      success: manageData
    })
  }

  $('#invoke-ajax-test-call').on('click', function() {
    var searchText = $('#searchText').val();                             // operatie sincrona; var query = $('[name="query"]').val(); 
    getShowsData(searchText)                                            // argument -> trimit valori
  })

  function manageData(data, textStatus, jqXHR) {
    $(".container").html(renderHtml(data))
  }

  function renderHtml(data) {
    var html = `<ul>`
    for (i = 0; i < data.length; i++) {
      var name =  data[i].show.name;
      var score = data[i].score;
      var url =  data[i].show.url;
      // console.log(data);
      html += `<li>` + name + " | " + score + " | " + `<a href="` + url + '"target="_blank">' + url + `</a>` + `</li>`      // `` -> transform in string 
    }

    html += `</ul>`
    return html;
  }
}