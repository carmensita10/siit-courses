document.addEventListener("DOMContentLoaded", onHtmlLoaded);

function onHtmlLoaded() {
  // Get language from cookie 
  var culture = getCookie('culture');
  // Get radio button that has the value equal to the value of the "culture" cookie
  var radio = document.querySelector('[value=' + culture + ']');
  // if such a radio button exists
  if (radio) {
      // check/tick that radio button 
      radio.setAttribute('checked', 'checked');
  }
  
  // when clicking on a radio button
  // get all radio buttons with name attribute = language
  var radios = document.querySelectorAll('[name=language]');
  radios.forEach(function(radio) {
    // add click event listeners for each radio button
    radio.addEventListener('click', function() {
      // this function is executed when we click on a radio button
      // in a click event handler, "this" is the element that was clicked on
//      console.log("ELEMENTUL CLICK ", this);
      // GET VALUE OF RADIO
     console.log("ELEMENTUL CLICK ", this);
     // this.value is "ro" or "en"
     // set the culture cookie with this.value
     document.cookie = "culture=" + this.value + "; expires=Thu, 18 Dec 2019 12:00:00 UTC; path=/";
    });   
  })
}


// COOKIES
// GET APPLICATION COOKIES
console.log("GET COOKIES ", document.cookie);
// SAVE NAME IN COOKIE
document.cookie = "name=Anca";
//SAVE UNAME IN COOKIE GLOBAL
document.cookie = "username=Anca; expires=Thu, 18 Dec 2019 12:00:00 UTC; path=/";
// REMOVE COOKIE - set expiration in the past
document.cookie = "temp=Anca; expires=Thu, 18 Dec 2019 12:00:00 UTC; path=/";
document.cookie = "temp=Anca; expires=Thu, 18 Dec 2000 12:00:00 UTC; path=/"; // set past date


getCookie("name");
getCookie("language");

// HELPER TO GET COOKIE VALUE
function getCookie(key) {
  var cookies = document.cookie.split('; '); // Array with key=value
  var response;
//   console.log("cookies as key=value", cookies);
  // Parse cookies to get value
  cookies.forEach(function(value){
//     console.log("Cookie elem ", value);  
    var cookie = value.split('=');
    if (cookie[0] === key) {
//       console.log("FOUND MY COOKIE ", cookie[1]);
        response = cookie[1];
    }      
  });
  return response;
}


