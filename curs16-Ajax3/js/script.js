$(onHtmlLoaded)

function onHtmlLoaded() {
  
 //  $.ajax("https://jsonplaceholder.typicode.com/posts");          // solicita request spre url

  var loadPosts = function () {
   $.ajax({                                                      // request js prin ajax
    url: "https://jsonplaceholder.typicode.com/posts",          // pasul 1: request spre URL. metoda recuperare date server -> recupeare resurse = GET
 // method: "GET"                                              // by default este GET si nu trebuie neaparat scris
    success: function(response) {                             // pasul 2: recuperare raspuns de pe request in script.js // in parametrul response, ajax trebuie sa imi puna lista recuperata de la server
      console.log("Posts = ", response);                     // cand s-a incarcat cu succes sa se intample ceva, in cazul asta afisare posturi in consola.
    }                                                       // vrem sa il afisam la user -> trebuie sa il trimit la o functie; metoda de call back
   });
  }

// Recuperare post cu id-ul dat de mine
  var loadPost= function (id) {
   $.ajax({
    url: "https://jsonplaceholder.typicode.com/posts/" + id,
    success: function(response) {                                      
      console.log("Post = ", response);
    }
   });
  }
  
 // Request to create a post
  var createPost = function() {
    $.ajax({
      url: "https://jsonplaceholder.typicode.com/posts",
      method: "POST",                                              // obligatoriu de ales metoda POST
      data: {                                                     // adaugare continut.-> body-ul se trimite prin atributul data. pe data salvez continutul pe care vreau sa il trimit                                                                  
        title: "Post by Carmen",
        body: "Bla by Carmen",
        userId: 2
      },
      success: function(response) {                                             // ma asigur ca s-a trimis ceea ce am vrut. sa vedem ce ne trimite requestul
        console.log("Create post ", response);                                 // ce returneaza functia in caz de success
        console.log("Create post with id ", response.id);                     //m response.id arata numarul posturlui
      }
    });
  }
  
// Request update a post
  var editPost = function (id) {
    $.ajax({
      url: "https://jsonplaceholder.typicode.com/posts/" + id,                   // cand punem "/" operatiune trebuie sa fie update, recupeare, delete
      method: "PUT",
      data: {
        title: "Edit post",
        body: "Edit body",
        userId: 1
      },
      success: function(response) {
        console.log("Updated post ", response);
      } 
    });
  }
  
  // Request delete post
    var deletePost = function(id) {
      $.ajax({
        url: "https://jsonplaceholder.typicode.com/posts/" + id,        // vreau sa sterg un post care exista si trebuie sa il idenfific-> id-> /
        method: "DELETE",   
        success: function() {                                         // nu am pus parametrul response ptr ca nu il folosim in functie.
          console.log("Post was deleted");
        }
      });
    }
    
 // Call-urile de functii se fac la final daca sunt salvate in variabile
    loadPost(3); 

    createPost();

    editPost(10);

    deletePost(4);
}
  