$(onHtmlLoaded)

function onHtmlLoaded() {
  var apiUrl = "https://jsonplaceholder.typicode.com/posts/";

  // Get post list 
  showTitle();                                                    // o apelam aici ptr ca este functie. Daca salvam rezultatul functiei intr-o variabila, aceasta trebuia chemata dupa declararea functiei          

// Exercitiu 1: stergere post cu id-ul dat de user in input
  // Pasul 1: Abonare eveniment click pe buton
  $("#deletePost").on("click", deletePostFunction);

 // Pasul 2: Request de tip ajax
  function deletePostFunction() {
    var id = $("[name=delete]").val();                          // vreau sa imi recupereze valoarea din input doar daca este dat click pe buton. vreau sa o recuperez doar unde o folosim.
    $.ajax({
      url: apiUrl + id,
      method: "DELETE",
      success: function() {
        alert("Post with id " + id + " deleted");
      }
    });
  }

// Exercitiu 2: Afisare titluri posturi in html
  // Pasul 1: functie care executa un request ajax care recupereaza titluriile posturiilor - GET
  function showTitle() {
    var listContainer = $("#listPost");                                   // identificare container care urmeaza a fi populat
    $.ajax({
      url: apiUrl,
      success: function(response) {
       //  console.log("Response", response);                         // returneaza toata lista cu posturi
       for (var i = 0; i < response.length; i++) {                   // avem o lista/array de elemente
        // console.log(response[i].title);                          // afiseaza titluriile posturilor in consola
        var title = "<h3>" + response[i].title + "</h3>"           // adaugare element html in container
        listContainer.append(title);                              // adaugare la lista container
       }
      }
    });
  }
}


/*  $('#deletePost').on('click', deletePostFunction) 

// Request delete post
 function deletePostFunction {
  var insertedID = $('#inputID').val();
  $.ajax({
   url: apiUrl + id,
   method: "DELETE",
   success: function() {
    console.log("Post was deleted");
   }
  });
 } */