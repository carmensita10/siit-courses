// define the parent/superclass
function Animal(options) {
  options = options || {};                            // this is used to avoid errors when calling a new Animal with no parameter(no options); eg., var animalObj = new Animal();  daca este gol ii face empty object         
  this.name = options.name;
  this.color = options.color;
  this.weight = options.weight;
  this.legs = options.legs;
}

Animal.prototype.eat = function() {
  console.log("nom, nom, animal is eating");
}

Animal.prototype.speak = function() {
  console.log("Hello, animal is speaking");
}

var animalObj = new Animal({
  name: "A generic animal",
  color: "purple"
});

console.log("animal Object", animalObj);

// define child class/subclass
function Dog(options) {                        
  Animal.call(this, {legs: 4});                // this line is used to inherit all properties from parent class.  Metoda .call face mostenire de proprietati, nu si de metode    
  /* sau                                      // obiectul ({}) reprezinta options. 
   {                                        
    Animal.call(this);                       // this = catelul nostru, obiectul pe care il creez;
    this.legs: 4;                           // legs = proprietatiile pe care toti cainii le vor lua. suprascrie clasa parent
   } 
  */
  this.breed = options.breed;                     
  this.owner = new Owner({               // composition relationship: has-a
    name: options.ownerName,                 
    age: options.ownerAge
  });
}                                                             

/*
var nero = new Dog({
  name: "Nero"
  color: "white"
});
  sau 
nero.color = "white";
console.log("Nero", nero);
*/

// console.log("nero can't speak yet", nero.speak());


Dog.prototype = Object.create(Animal.prototype);                 // next line is used to inherit all methods from parent class; ne intereseaza sa mostenim si ce avem pe prototip, nu doar pe constructor function. 

var nero = new Dog({name: "Nero"});
  nero.name = "Nero",
  nero.color = "white";
  console.log("Nero", nero);
  console.log("nero can speak now");
nero.speak();

// method overriding
Dog.prototype.speak = function() {
  console.log("Ham, ham!");
}
nero.speak();
nero.eat();
animalObj.speak();

// Composition - define Owner class
function Owner(options) {
  options = options || {};
  this.name = options.name;
  this.age = options.age;
}

var rex = new Dog ({
  ownerName: "Carmen",
  ownerAge: 25
});

console.log("rex", rex);
console.log("Rex's owner name: ", rex.owner.name);                     // owner??? name???

// Polymorphism
function Cat(options) {
  Animal.call(this, {legs: 4});
}
Cat.prototype = Object.create(Animal.prototype);           
Cat.prototype.speak = function() {
  console.log("Miau Miau!");
}

var tommy = new Cat();                    
tommy.name = "Tommy";

console.log("-----------------");
var animalArray = [tommy, rex, nero, animalObj];
for (var i = 0; i < animalArray.length; i++) {
  var animal = animalArray[i];
  animal.speak();  
}

