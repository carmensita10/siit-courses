  $(onLoad)                                                                          // primul callback ptr functia $(dolar/functie de incarcare DOM) . //listener

  function onLoad() {                                                               // functie care se executa doar atunci cand pagina este incarcata 
    
    $('#button1').on('click', function() {                                         // al doilea callback ptr evenimentul de click pe buton
      $('#spinner').css('visibility', 'visible');                                      
      
      getName(function(fullName) {                                               // al treilea callback ptr functia getName // apelare functie getName. se executa liniile 1-15 din library si apoi spinner-ul
        $('#spinner').css('visibility', 'hidden');                                    
        $('.content').html('Numele complet este: ' + fullName)                         
      })
    })

 // Promise function
    $('#button2').on('click', getPosts)
      
    function getPosts() { 
      $.ajax({
        url: "https://jsonplaceholder.typicode.com/posts",
      })
      .then(function(result) {
        console.log('Post: ', result);
      /* 
      nu functioneaza
            varianta 1
        var listContainer = $(".content");
        for(var i = 0; i < result.lengh; i++) {
          var html = '<h4>' + result[i].title  + '</h4>';
          listContainer.append(html);
        }
        
            varianta 2
        var htmlString = '<ul>';
        for(var i = 0; i < result.lengh; i++) {
          htmlString += '<li>' + result[i].title + '</li>';
        }
        htmlString += '</ul>';
        $('.content').html(htmlString);
      */
      });
    }

  }

/* var callBack = (function(fullName) {                                       // callBack este o functie callback
    console.log('fullName!!!');
   });
   
   getName(callBack);                                                       // aici o sa execute codul din functia fullName 

   function test() {
    // code here
   }                                            

   var name = getName();                                                  // apel functie(getName() ); recuperare valoare din functie getName in var name
   alert('Full name is: ' + name);                                       // alertul se incarca mai repede decat functia getName din cauza delay-ului, iar din cauza asta primim undefined 
                                                                        // solutia este callback-> cand se termina de incarcat functia getName, arata-mi alertul sau ce doresc sa fie afisats
   getName(function(name2) {                                           // getName este functie asincrona si niciodata nu o sa pot recupera rezultatul in ea si de aia avem nevoie de call back->
     alert('a doua implementare cu numele '+ name2)
   })

    getName(test)                                                  // aici o sa execute codul din functia test
*/

