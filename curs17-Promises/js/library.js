function getName(callback) {                                           // functia getName
    var firstName = prompt("First Name")
    var lastName = prompt("Last Name")
    var fullName                                                        

    setTimeout(function() {                                                // setTimeout(function() {}, 2000) sintaxa functie
      fullName = firstName + ' ' + lastName                               // intarziere cu 2 secunde executarea functiei getName
      if (typeof callback === 'function') {                              // typeof este o verificare=> daca variabila este o functie si daca exista                            
          callback(fullName)                                            // in felul asta putem face alert si console.log
      } else {
          console.log('callback is not defined or is not a function!!!!')
      }                                                                 
    }, 2000)
  }