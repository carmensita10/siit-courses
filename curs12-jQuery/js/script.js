// DOM Loaded
  $( function() {                                 // jQuery primeste ca si prametru o functie
    
    });
  $(onHtmlLoaded);  

  function onHtmlLoaded() {
    console.log("On HTML Loaded");
    
// Identificare elemente 
    
    // by id - jQuery element
      console.log("Comments List", $("#comments-list"));
    // by id - first by id
      console.log("Comments List native", $("#comments-list")[0]);
    // by class
      console.log("Comment Item", $('.comment-item'));
    // by tag
      console.log("Header", $('h1'));
    // by query selector
      console.log("Author", $('section.comment-item strong'));
   
    
// Change h1 content
   var h1 = $ ('h1');
   h1.text('jQuery DOM MANIPULATION');
   h1.html('jQuery DOM MANIPULATION <em> New </em>');
   
    
// Set Attribute   
   var h2 = $('h2');                                                          // set attr
   h2.attr('title', 'Comments List - Set Attribute');
    
    
// Recuperare atribut
   console.log ("H2 title recuperare ->", h2.attr('title'));                 // get attr
    
    
// Change style  
   var commentsContainer = $('#comments-list');
   commentsContainer.css('border', 'solid 1px green');
   commentsContainer.css( {
      color: 'blue',
      background: '#ccc'
    });
   console.log("Comments list border recuperare->", commentsContainer.css('border'));       // recuperare css
    
    
// Add border to each comment
   var commentItems = $('.comment-item');                                                 // daca folosim obiectul de cel putin 2 ori, trebuie pus intr-o variabila
   commentItems.css('border', 'solid 5 px pink');                                        // noi il folosim aici si la remove si de aceea l-am pus intr-o var.
// Add border to first comment
   $('.comment-item').first().css('border', 'solid 5 px pink');          
    
    
// Add new DOM element -> comentariu nou
    
  // Pasul 1: Adaugare continut
      var comment = '<section>' +                                                          // am creat o variabila doar ca sa fie mai clar
                      '<h3> Comment 3 </h3>' +
                      '<p> Content <strong> Daniel Rus </strong>' +
                    '</section>'
  // Pasul 2: localizare
      commentsContainer.append(comment);

    
// Workshop - Add comments list
    
   var list = [    
     {
       title: "Title 1",
       body: "Lorem ipsum...",
       author: "Ioan Pop"
     }, 
      {
       title: "Title 2",
       body: "Lorem ipsum...",
       author: "Lorena Pop"
     }, 
      {
       title: "Title 3",
       body: "Lorem ipsum...",
       author: "Daniel Bolog"
     }
   ];
   
    
  // Parcurgere lista  
    for(var i=0; i < list.length; i++) {
      var item = list[i]                                                                      // ca sa avem obiectul curent am facut var item
      comment = '<section>' +                                                          
                  '<h3>' + item.title + '</h3>' +                                            // item.title => variabila din obiect
                  '<p>' + item.body + ' <strong>' + item.author + '</strong>' +
                '</section>'
      commentsContainer.append(comment);
    }

    
  // Remove -> identificare element si apelare metoda remove  
     commentItems.first().remove();
    
    
  // EVENTS 
    // Bind events - > ABONARE DE EVENIMENTE
      var btnSearch = $('#btn-search');
      btnSearch.on('click', function() {                                             // Functie call back-> ce sa se intample cand dam click pe buton
        // get search value
        var search = $('input[name="searchInput"]').val();                          //.val recupereaza valoarea introdusa in input
        console.log("Search value", search);
      });
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  }