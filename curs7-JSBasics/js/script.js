// Play with literals (values)
console.log("Number = ", 12)                        // "number" este string
console.log("String = ", 'Carmen')                // Carmen este string

var age = 20;                                   // punem var ca sa o facem locala
console.log("Age = ", age);                    //age = aratam ce reprezinta age/valoarea

var name = "Carmen";
console.log("Name =", name);

var isTrue = true;
var isFalse = false;
console.log("isTrue = ", isTrue);
console.log("isFalse = ", isFalse);

var thisIsNull = null;
console.log("thisIsNull = ", thisIsNull);

var thisIsNotDefined;
console.log("thisIsNotDefined = ", thisIsNotDefined);

    // Operators

// Operatiuni matematice
console.log("Sum = ", 2 + 2); 
var a = 10;
var b = 4;
var c = a-b;
console.log("Diff = ", c);

// Operatori de comparatie
console.log("Compare ", 2 > 0);
console.log("Compare ", 0 <= 2);

// logical operaters - o sa le folosim des
console.log("Logical  &&", 2 > 0 && 0 > 2);
console.log("Logical ||", 2 > 0 || 0 > 2);

var logical = ( 0 > 2);                         // 0 is NOT ! bigger then 2
//console.log("Logical !", !0 > 2);
console.log("Logical !", !logical);

// Concatenare 
var fname = "Anca";
var lname = "Balc";
var fullname = fname + " " + lname;           // concatenarea se face cu +
                                             // " " Anca Balc. fara spatiu ar fi fost AncaBalc
                                            // " " string
console.log("Full Name = ", fullname);

    //Array

// Empty array
var a = [];
console.log("a = ", a);

// Populated array
var a = ["Ana", "Ion"];
console.log("a = ", a);
console.log("Array length is: ", a.length);       // returneaza cate elemente are array-ul meu.

// Adaugare valorea in array
a.push("Gheorghe");
a.push(50);
a.push([10]);                                   // am adaugat un array in array-ul nostru
console.log("a = ", a);

console.log("a = ", a);
console.log("a = ", a[4]);                    // returneaza valoarea din array care are indexul 4. adica [10] 
console.log("a = ", a[4][0]);                // returneaza valoarea care are indexul 0 din arrayul cu indexul 4, adica 0

var mix = [1, 'two', ['apple', 'orange']];
console.log(mix[2]);

var fruits = ['Apple', 'Banana'];
console.log(fruits.length-1);









