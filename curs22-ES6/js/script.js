test();                                                           // test function is hoisted so we can call it before it's defined in the code

// console.lor("str in global scope",str);                     // reference eroare -> suntem in global scope, iar str nu este declarat si nici definit aici.

function test() {
  // var str -> hoisted. ca si cum s-ar muta aici ptr ca este var si este declrata in functia test (mai poate fi si let si const). // it is hoisted from the first line in for
  console.log("str in test function", str);                // undefined ptr ca nu are atribuita valoare. valoarea este atribuita doar la linia 10

  for (var i = 1; i < 10; i++) {
    var str = "Something" + i;                           // si aici ar fi scris asa: str = "something" + i. Partea asta mereu ramane aici
    console.log(str);
  }
}

// test();                                         // functioneaza si aici

// myfunction();                                 // reference error ptr ca doar var myfunction se muta mai sus, dar nu si declararea
var myfunction = function() {                   // intrebare interviu
  console.log("my function");
};

/*
under the hood

var myfunction; 
myfunction();                     // apel functie
myfunction = function(){         // declararea/assigment 
  console.log("my function");
}
*/

// LET si CONST
const mynumber = 2;
//mynumber = 7;                     // error -> nu se poate reasigna o valoarea la o variabila constanta

const user = {
  name: "john",
  age: 35
};

/*
user = {
  name: "mary",
  age: 35
};                            // error: assigment to constant variable
*/

user.name = "Mary";
console.log("user", user);

