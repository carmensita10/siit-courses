// Empty object
var robot = {};
console.log("Robot = ", robot);

var robot = {
  model: "RT000",
  weight: 100                                       // nu se pune "," dupa ultima valoare. poate genera eroare.
};
  console.log("Robot = ", robot);


// Adaugare proprietate noua
robot.color = "red";                              // daca nu exista proprietatea culoare acesta o adauga. Daca exista, o suprascrie/modifica
console.log("Robot = ", robot);


// Update property
robot.weight = 99;
console.log("Robot = ", robot);


// Property delete
// delete robot.model;
// console.log("Robot = ", robot);


// adaugare metoda in obiect
var robot = {
  model: "RT000",
  weight:100,
  run: function(){
    console.log("Robot is running");
  }
};
  console.log("Robot = ", robot);
  robot.run();                                            // robot.run() -> asa se apleaza o functie, cu(). 
                                                         // afiseaza direct Robot is running
                                                        // robot.run -> afiseaza ce am scris noi in console,log, fara a fi apelat

//type of
var name = "Carmen";
console.log("Type of string", typeof name);

var age = 10;
console.log("Type of string", typeof age);

var isNull = null;
console.log("Type of string", typeof isNull);

var isUndefined;
console.log("Type of string", typeof isUndefined);

var list = [];
console.log("Type of array", typeof list);


// IF statement
  var x = 10;                                              // o sa apara mesajul doar daca x este pozitiv 
  if (x > 0) {                                            // daca x este numar pozitiv afiseaza ce scrie mai jos
    console.log("Positive number x = ", x);              // afiseaza numarul
  } else {
     console.log("Negative number x = ", x);
  }

  var x = 0;
  if (x > 0) { 
    console.log("Positive number x = ", x);
  } else if (x === 0){
     console.log("x = ", x);
  } else {
     console.log("Negative number x = ", x);
  }

  // afisare prompt
  var currentTime = prompt ("Enter time");              // este un fel de alert, dar are input in plus
  console.log(currentTime);

  if (currentTime < 10) {
    console.log("Buna dimineata!");
  } else if (currentTime <= 18) {
    console.log("Buna ziua!");
  } else {
    console.log("Buna seara!");
  }

// Structra repetitiva
for (var i = 0; i <= 10; i++ ) {                        // folosim "var" ca sa o facem locala
  console.log("Index = ", i);
}

// Sirul lui fibonacci
for (var i = 1; i <= 5; i++) {                       // primul for reprezinta numarul de linii, care vor fi 5 in total
  var line = " ";                                   // inserare rand gol
  for (var j = 1; j <= 5; j++){                    // al doilea for reprezinta numarul de coloane
    line += (i * j) + " ";                        // la randul anterior adauga rezultatul inmultirii a lui i si j.
  }
  console.log("Line ", line);
}







