// apelare functii
// fisier de view
// toate manipulariile de html
// de html se ocupa view
// view face legatura intre date si html. view instantiaza modelul ca sa fac apel de getAll dupa care apeleaza metoda dispalyAllArticles


window.addEventListener("load", function() {                             // tot codul se executa dupa ce s-a incarcat pagina
  var containerEl = document.getElementById("articles-list");           // prima data luam containerul care contine ul.

  var articlesModel = new Articles();                                  // ca sa facem call la API trebuie sa instantiem model. instantiem un articlesModel. nu putem accesa Articles.getAll si de aceea trebuie sa facem o instantiere a obiectului articlesModel. clasa poate fi accesata doar prin mecanismul prototype                  
  articlesModel.getAll().then(function(response) {                    // pe then apelam  o functie care se numeste displayAllArticles
                                                                     // pe articlesModel punem functia getAll().  response este rezultatul functiei articlesModel.getAll(), care returneaza 100 posturi si care sunt salvate in response    
    displayAllArticles(response);                                   // call displayAllArticles function with the response from API // exactly the same as: articlesModel.getAll().then(displayAllArticles);    
  });

  function displayAllArticles(articlesData) {
    // console.log(articlesData);
    for (var i = 0; i < articlesData.length; i++) {
                                                             // ptr fiecare articol am facut o instanta, ca sa salvam datele primite pe acea instanta -> article
      var article = new Article(articlesData[i]);           // ceea ce primim de la API il punem intr-o clasa Article. article este o instanta a clasei Article. model de tip article care are acees la datele si functionalitatiile din Article.js             
      displayArticle(article);                             // am afisat fiecare articol in parte -> venit din functia displayArticle
    }
  }

  function displayArticle(article) {                        // article de aici este o variabila primita de mai sus, care trebuie afisata in lista
    var liEl = document.createElement('li');

    var titleEl = document.createElement('h1');
    titleEl.innerHTML = article.title;                      // var article = new Article(articlesData[i]) -> acest article
    liEl.appendChild(titleEl);
    
    titleEl.addEventListener('click', function() {         // ca sa nu cream 100 de pagini html ptr fiecare articol in pagina, folosim articleId -> articleId identificator unic
      window.location = "http://cursuri-carmenandreea10688716.codeanyapp.com/curs20-OOP-Workshop/templates/article.html?articleId=" + article.id;           // du-ma pe pagina article.html, respecta strutura aia si intra pe articolul cu id curent si afiseaza articolul detaliat                    
    });

    var bodyEl = document.createElement('p');
    bodyEl.innerHTML = article.body;
    liEl.appendChild(bodyEl);
   
    containerEl.appendChild(liEl);
  }

});