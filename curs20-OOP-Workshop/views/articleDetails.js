// pagina din care va fi creata fiecare articol

window.addEventListener("load", function() {
  var containerEl = document.getElementById("article-details");

  var article = new Article();                                            // instantiere article. nu ii trimitem nicio proprietate = 4 proprietati undefined/ options undefined. vreau functionalitatea lui           
  var articleId = getUrlParameter("articleId");                          // punem in query URL un articleId                    
  article.id = articleId;                                               // article.id 

  article.getArticleDetails().then(function() { 
    displayArticleDetails(article);
  });

  function displayArticleDetails(articleDetails) {                  // articleDetails este doar un nume, nu trebuie sa pui article care contine datale articolului curent
    var titleEl = document.createElement('h1');
    titleEl.innerHTML = articleDetails.title;
    containerEl.appendChild(titleEl);

    var bodyEl = document.createElement('p');
    bodyEl.innerHTML = articleDetails.body;
    containerEl.appendChild(bodyEl);
  }



  // apel API. vreau sa stiu ce articol vreau sa fie afisat => id in url -> templates/articles.html?articleId=2  
 // This method returns the value of a specified URL parameter from a query string, e.g. if the URL is "...?id=5" then calling getURLParam("id") will return "5".
  /**
   * It retrieves a query (URL) parameter value
   * 
   * It expects you to send the parameter key(before '=')
   */
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);                // location.search = query URL, citeste url si ia valoarea query param pe care il dam  
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  }

});