// pagina ptr articolele individuale
// ne uitam in documentatia API-ul sa vedem ce ne returneaza => punem datele care ne intereseaza in modelul nostru

function Article(options) {
  options = options || {};
  this.id = options.id;
  this.title = options.title;
  this.body = options.body;
  this.userId = options.userId;
}
    
Article.prototype.getArticleDetails = function () {                                     // metoda pusa pe prototype care va fi valabila ptr toate instantele clasei Article     
                                                                                       // we are saving the value of the current context(=article model)
                                                                                      // later on,  in the AJAX succes function we will set attributes on it
  var that = this;                                                                   // this in interiorul unei functii promises sau intr-un event listener nu are legatura cu functia constructor.             
  return $.get('https://jsonplaceholder.typicode.com/posts/' + this.id)             // metoda nu o vedem pe obiect, ci pe prototype
    .then(function(response) {
        that.title = response.title;                                          
        that.body = response.body;
        that.userId = response.userId;
       // console.log("this", this);                                           // ca sa vedem diferenta dintre ele
       // console.log("that", that);
  });
}